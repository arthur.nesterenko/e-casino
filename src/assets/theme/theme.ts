export interface IColors {
    green: string;
    lightGreen: string;
    lightBlue: string;
    white: string;
    critical: string;
    bg: string;
    dark: string;
    blue: string;
    danger: string;
    brand: string;
    darkBlue: string
}

export interface ITheme {
    colors: IColors

}


const theme: ITheme = {
    colors: {
        lightBlue: '#5DA5ED',
        brand: '#133252',
        blue: '#275789',
        white: '#fff',
        green: '#73FC7F',
        lightGreen: '#6FF48D',
        critical: '#FD7979',
        danger: '#FA6868',
        bg: '#525252',
        dark: '#292929',
        darkBlue: '#0A1A33'
    }


}

export default theme
