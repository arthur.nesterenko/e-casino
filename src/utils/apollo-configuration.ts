import {HttpLink} from "apollo-link-http";
import {WebSocketLink} from "apollo-link-ws";
import {split} from "apollo-link";
import {getMainDefinition} from "apollo-utilities";
import ApolloClient from "apollo-client";
import {InMemoryCache} from "apollo-cache-inmemory";

const httpLink = new HttpLink({
    uri: process.env.REACT_APP_API_URI
});

const wsLink = new WebSocketLink({
    uri: process.env.REACT_APP_WS_URI,
    options: {
        reconnect: true
    }
} as any);

const link = split(
    // split based on operation type
    ({query}) => {
        const definition = getMainDefinition(query);
        return (
            definition.kind === 'OperationDefinition' &&
            definition.operation === 'subscription'
        );
    },
    wsLink,
    httpLink,
)

const client = new ApolloClient({
    link,
    cache: new InMemoryCache()
});

export default client
