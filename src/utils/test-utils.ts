import {createElement, FunctionComponent} from 'react'
import {render} from '@testing-library/react';
import {ThemeProvider} from 'styled-components';
import theme from './../assets/theme/theme';

const AllTheProviders: FunctionComponent<any> = ({children}) => {
    return createElement(ThemeProvider, {children, theme})
}

const customRender = (ui: any, options: any) =>
    render(ui, {wrapper: AllTheProviders, ...options});

// re-export everything
export * from '@testing-library/react';

// override render method
export {customRender as render};
