import React, {Fragment} from 'react';
import Header from "shared/components/header";
import BetTable, {IBet} from "./shared/components/bet-table";
import {Main, Section} from './shared/components/base'

import Slider from "./shared/components/slider";
import BetInput from "./shared/components/bet-input";
import Clam from './shared/components/clam'
import useBetSubscription from "./shared/hooks/use-bet-subscription";

const App = () => {
    const [val, setVal] = React.useState({value: 20})
    const [inputVal, setInputVal] = React.useState<number | undefined>(undefined)
    const {bets} = useBetSubscription<IBet>({});

    return (
        <Fragment>
            <Header/>
            <Main data-testid='main'>
                <Section style={{justifyContent: 'space-between'}}>
                    <Slider {...val} onChange={setVal}/>
                    <BetInput name='bet' onChange={(e) => setInputVal(+e.currentTarget.value)} placeholder='0'
                              value={inputVal}/>
                </Section>
                <Section>
                    <BetTable items={bets}/>
                </Section>
                <Section>
                    <Clam/>
                </Section>
            </Main>
        </Fragment>
    )
}

export default App;
