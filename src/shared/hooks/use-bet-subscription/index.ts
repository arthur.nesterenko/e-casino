import gql from 'graphql-tag';
import React, {useState} from "react";
import {useSubscription} from "@apollo/react-hooks";

export const BET_SUBSCRIPTION = gql`
    subscription{
        betAdded{
            id
            time
            bet
            payout
            profit
        }
    }`


const MAX_ROWS = 10;

interface IUseBetSubscriptionOptions {
    maxBets?: number
}

interface IUseBetSubscriptionResult<T> {
    loading: boolean,
    bets: T[]

}

const useBetSubscription = <T>({maxBets = MAX_ROWS}: IUseBetSubscriptionOptions): IUseBetSubscriptionResult<T> => {

    const [bets, addBet] = useState<T[]>([])
    const {data, loading} = useSubscription<{ betAdded: T }>(BET_SUBSCRIPTION)

    React.useEffect(() => {
        if (data && !loading) {
            const {betAdded} = data;

            addBet(p => {
                const isOverflow = p.length === maxBets;
                return [betAdded].concat(isOverflow ? p.slice(0, -1) : p)
            })
        }
        // eslint-disable-next-line
    }, [data, loading]);


    return React.useMemo<IUseBetSubscriptionResult<T>>(() => ({loading, bets}), [bets, loading])

}


export default useBetSubscription;
