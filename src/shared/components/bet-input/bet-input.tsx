import React, {FunctionComponent} from 'react'
import BitcoinIcon from "../icons/bitcoin-icon";
import {BetInputContainer, Button, Input, InputContainer, InputRow, Title} from './bet-input.styled'

interface IBetInput {
    name: string,
    placeholder?: string,
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
    value?: number
}

const BetInput: FunctionComponent<IBetInput> = (props) => {

    const onKeyPress = React.useCallback((e: React.KeyboardEvent<HTMLInputElement>) => {
        if (isNaN(+e.key)) {
            return false;
        }
    }, [])

    return <BetInputContainer>
        <InputContainer>
            <Title>Bet Amount</Title>
            <InputRow>
                <BitcoinIcon/>
                <Input onKeyPress={onKeyPress} type="number" {...props}/>
            </InputRow>
        </InputContainer>
        <Button>1/2</Button>
        <Button>x2</Button>
    </BetInputContainer>
}


export default BetInput;
