import styled from "styled-components";

export const BetInputContainer = styled.div`
display: grid;
grid-template-columns: minmax(0, 1fr) repeat(2, 58px);
align-items: center;
background-color: ${p => p.theme.colors.darkBlue};
border-radius: 8px;
height: 57px;
overflow: hidden;
box-sizing: content-box;


 &:focus-within,
  &:focus,
  &:active,
  &:hover{
    box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
  }

`

export const Input = styled.input`
background: transparent;
border: none;
outline: none;
color:${p => p.theme.colors.white};
font-size: 20px;
-moz-appearance: textfield;
&::-webkit-inner-spin-button,
&::-webkit-outer-spin-button{
-webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  margin: 0;
  transition: all 0.3s cubic-bezier(.25,.8,.25,1);
}
&::placeholder{
color:${p => p.theme.colors.white};
}
`

export const Button = styled.button`
background-color: #275E9A;
display: flex;
align-items: center;
justify-content: center;
height: 100%;
width: 58px;
color:${p => p.theme.colors.white};
font-size:14px ;
outline: none;
border: none;
transition: opacity .1s ease-in-out .1s;
cursor: pointer;
&:hover{
opacity: 0.8; 

}
`;

export const Title = styled.span`
text-transform: uppercase;
color:#80A2CC;
font-size: 14px;
display: inline-block;
`;

export const InputContainer = styled.div`
padding: 7px 10px;

`

export const InputRow = styled.div`
 display: grid;
 align-items: center;
 grid-gap: 10px;
 grid-template-columns: min-content 1fr;
`
