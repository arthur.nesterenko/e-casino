import React, {FunctionComponent, ChangeEvent} from 'react'
import {getClientPosition} from './utils'
import {Container, Range, SliderContainer, Thumb, ThumbValue, Values,} from './slider.styled'
import {ISlider, IPosition, IOffset} from './types'

const TOP_MAX: number = 100;
const TOP_MIN: number = 0;


const Slider: FunctionComponent<ISlider> = ({onChange, min = 0, max = 100, step = 1, value}) => {

    const [y, setY] = React.useState<number>(() => Math.abs(value - max));
    const container = React.useRef<any>(null);
    const handle = React.useRef<any>(null);
    const start = React.useRef<IOffset>({x: 0, y: 0});
    const offset = React.useRef<IOffset>({x: 0, y: 0});

    function getPosition(): number {
        let top: number = ((y - min) / (max - min)) * TOP_MAX;
        if (top > 100) top = TOP_MAX;
        if (top < 0) top = TOP_MIN;
        return top
    }


    function change({top,}: IPosition): void {
        if (!onChange) return;

        const {height} = container.current.getBoundingClientRect();
        let dy = 0;

        if (top < 0) top = 0;
        if (top > height) top = height;

        dy = (top / height) * (max - min);


        const y = (dy !== 0 ? (dy / step) * step : 0) + min;

        setY(y)
        onChange({value: Math.abs(max - y)});
    }


    function handleMouseDown(e: ChangeEvent<any>) {
        console.log('DOWn')


        e.preventDefault();
        const dom = handle.current;
        const clientPos = getClientPosition(e);

        start.current = {
            x: dom.offsetLeft,
            y: dom.offsetTop
        };

        offset.current = {
            x: clientPos.x,
            y: clientPos.y
        };


        document.addEventListener('mousemove', handleDrag);
        document.addEventListener('mouseup', handleDragEnd);
        document.addEventListener('touchmove', handleDrag, {passive: false});
        document.addEventListener('touchend', handleDragEnd);
        document.addEventListener('touchcancel', handleDragEnd);
    }

    function getPos(e: any) {
        const clientPos = getClientPosition(e);
        const top = clientPos.y + start.current.y - offset.current.y;
        return {top};
    }

    function handleDrag(e: any) {

        e.preventDefault();
        change(getPos(e));
    }

    function handleDragEnd(e: any) {

        e.preventDefault();
        document.removeEventListener('mousemove', handleDrag);
        document.removeEventListener('mouseup', handleDragEnd);

        document.removeEventListener('touchmove', handleDrag, {
            passive: false
        } as any);
        document.removeEventListener('touchend', handleDragEnd);
        document.removeEventListener('touchcancel', handleDragEnd);

    }

    function handleClick(e: DragEvent) {
        
        const clientPos = getClientPosition(e);
        const rect = container.current.getBoundingClientRect();

        change({
            top: clientPos.y - rect.top
        });


    }


    const topPos = getPosition();


    return <Container>
        <Values>
            <span>{max}</span>
            <span>{min}</span>
        </Values>
        <SliderContainer
            onClick={handleClick}
            data-testid='slider-container'
            ref={container}>
            <Range height={topPos}>
                <Thumb
                    data-testid='slider-thumb'
                    style={{
                        top: `${topPos}%`
                    }}
                    onTouchStart={handleMouseDown}
                    onMouseDown={handleMouseDown}
                    ref={handle}
                    onClick={function (e: any) {
                        e.stopPropagation();
                        e.nativeEvent.stopImmediatePropagation();
                    }}
                >
                    <ThumbValue>
                        {value.toFixed(2)}
                    </ThumbValue>
                </Thumb>
            </Range>
            <input type="hidden"
                   name="volume"
                   value={value}

            />
        </SliderContainer>
    </Container>
}


Slider.defaultProps = {
    onChange: () => undefined,
    min: 0,
    max: 100,
    step: 1,
}

export default Slider;
