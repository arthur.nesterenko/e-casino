import React from 'react'
import {render, getByTestId, fireEvent} from '../../../utils/test-utils'
import Slider from './slider'
import {ISlider} from './types'


describe('Slider component', () => {


    it('Should render correct min and max', () => {

        const props: ISlider = {
            value: 20,
            min: 0,
            max: 100,
            onChange: () => {
            },
            step: 1
        };
        const {getByText} = render(<Slider {...props}/>)
        expect(getByText(String(props.min))).toBeInTheDocument()
        expect(getByText(String(props.max))).toBeInTheDocument()

    })

    it('Should fire onChange event ', () => {
        const onChange = jest.fn()
        const {container} = render(<Slider onChange={onChange} value={30}/>)
        const sliderContainer = getByTestId(container, 'slider-container')
        fireEvent.click(sliderContainer, {});
        expect(onChange).toHaveBeenCalled()
    })
})
