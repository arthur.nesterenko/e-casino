import styled from "styled-components";
import union from "./assets/Union.svg";


export const Container = styled.div`
display: flex;
height: 290px;
`;

export const Values = styled.div`
color:${p => p.theme.colors.white};
  font-size: 50px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  text-align: right;
  margin-right: 30px;

`


export const SliderContainer = styled.div<any>`
  width: 107px;
  height: 100%;
  background: ${p => p.theme.colors.danger};
  position: relative;  
 
`

export const ThumbValue = styled.span`
font-size: 27px;
text-align: center;
color: ${p => p.theme.colors.white};
`


export const Thumb = styled.div<any>`
position: absolute;
transform: translateY(-35px);
right: 0;
 background-image:url("${union}");
background-repeat: no-repeat;
background-size: 100% 100%;
height: 70px;
width: 100%;
margin:0;
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
z-index: 2;
&:hover{
cursor: pointer;
}

${ThumbValue}{
transform: translate(10px);
}

`;

export const Range = styled.div<any>`
background-color:${p => p.theme.colors.lightGreen};
height:${p => p.height}%;
width: 100%;
`


