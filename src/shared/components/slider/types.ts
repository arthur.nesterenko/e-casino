export interface IOnChangePayload {
    value: number
}

export interface ISlider {
    onChange: (payload: IOnChangePayload) => void,
    min?: number,
    max?: number,
    step?: number,
    value: number
}

export interface IPosition {
    top: number,
    left?: number
}
export interface IOffset {
    x: number,
    y: number
}
