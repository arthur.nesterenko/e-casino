import React from 'react'
import {ReactComponent as BtcIcon} from './../../../assets/icons/btc-brands.svg'
import styled from 'styled-components'

interface IBitcoinIcon {
    size?: number
}

const Icon = styled.i<{ size: number }>`
margin-right: 5px;
background-color: #FEC41C;
width: ${p => p.size}px;
height:${p => p.size}px;
border-radius: 100%;
display: flex;
justify-content: center;
    align-items: center;
    svg{
    transform: rotate(30deg);
    }
`;

const BitcoinIcon = ({size = 11}: IBitcoinIcon) => {
    const iconSize = Math.ceil(size / 2)
    return <Icon size={size}>
        <BtcIcon width={iconSize} height={iconSize}/>
    </Icon>
};


export default BitcoinIcon;
