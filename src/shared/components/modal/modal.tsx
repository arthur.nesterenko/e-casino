import React, {FunctionComponent, ReactNode} from 'react'
import StyledModal, {BoxContent, BoxDialog, BoxHeader, Background, CloseButton} from './modal.styled'
import {createPortal} from 'react-dom'


const modalRoot: HTMLElement = document.createElement('div')
modalRoot.setAttribute('id', 'modal-root')
document.body.appendChild(modalRoot);


interface IModal {
    isOpen?: boolean,
    onClose: () => void,
    modalClass?: string,
    id?: string,
    title?: ReactNode
}

const Modal: FunctionComponent<IModal> = ({children, onClose, isOpen = false, modalClass, id, title}) => {

    const [status, setStatus] = React.useState<null | 'in' | 'out'>(null);

    const el = document.createElement('div')

    React.useEffect(() => {
        modalRoot.appendChild(el);
        return () => {
            modalRoot.removeChild(el);
        }
        // eslint-disable-next-line
    }, []);


    React.useEffect(() => {
        window.addEventListener("keydown", onEscKeyDown, false);
        const timeoutId = setTimeout(() => setStatus('in'), 0);

        return () => {
            window.removeEventListener("keydown", onEscKeyDown, false);
            clearTimeout(timeoutId)
        }
    }, []);

    React.useEffect(() => {
        if (isOpen === false) {
            setStatus('out')
        }
    }, [isOpen]);

    const onEscKeyDown = (e: KeyboardEvent) => {
        console.log('onEscKeyDown');
        if (e.key === "Escape") {
            setStatus('out')
        }
    }

    const handleClick = (e: React.MouseEvent<HTMLElement>) => {
        console.log('Click')
        e.preventDefault();
        setStatus('out')
    };


    const onTransitionEnd = (e: React.TransitionEvent) => {
        console.log('dd', e.propertyName)
        if (e.propertyName !== "opacity" || status === "in") return;

        console.log('hello', status)
        if (status === "out") {
            onClose();
        }
    };


    return createPortal(<StyledModal
        id={id}
        className={`wrapper fade-${
            status
        } ${modalClass}`}
        role="dialog"
        data-testid='modal-container'
        onTransitionEnd={onTransitionEnd}
    >
        <BoxDialog>
            <BoxHeader>
                {title}
                <CloseButton data-testid='modal-close' onClick={handleClick}>
                    ×
                </CloseButton>
            </BoxHeader>
            <BoxContent>{children}</BoxContent>
        </BoxDialog>
        <Background onMouseDown={handleClick}
        />
    </StyledModal>, modalRoot)

};


export default Modal;
