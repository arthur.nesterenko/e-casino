import React from 'react'
import {render} from '../../../utils/test-utils'
import Modal from "./modal";


const modalRoot = document.createElement('div')
modalRoot.setAttribute('id', 'modal-root')
document.body.appendChild(modalRoot)
describe('Modal component', () => {


    it('Should  be opened', () => {
        const onClose = jest.fn();
        const {getByText} = render(<Modal isOpen={true} onClose={onClose} title='Modal window'/>)
        expect(getByText('Modal window')).toBeInTheDocument()
    })


})
