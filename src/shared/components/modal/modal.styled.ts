import styled from "styled-components";

const Modal = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  opacity: 0;
  transition: opacity linear 0.15s;
  z-index: 2000;
 max-width: 800px;
  width: 100%;
  margin: 40px auto;
  
  
  &.fade-in {
    opacity: 1;
    transition: opacity linear 0.15s;
  }
  &.fade-out {
    opacity: 0;
    transition: opacity linear 0.15s;
  }
 
`;


export const BoxDialog = styled.div`
   z-index: 1050;
    width: 100%;
    background-color: #fefefe;
    box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
`

export const BoxContent = styled.div`
  padding: 24px;    
  background-color: #2D4560;
  min-height: 250px;
`;


export const BoxHeader = styled.div`
height: 48px;
      padding: 8px 24px;
      display: flex;
      justify-content: space-between;
      align-items: center;
      background-color: #113352;
      font-size:50px;
       color:${p => p.theme.colors.white};
      
     
`


export const Background = styled.div`
 background: rgba(0, 0, 0, 0.5);
    position: fixed;
    z-index: 1040;
    display: block;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    outline: 0;
`

export const CloseButton = styled.button`
  outline: none;
  border:none;
  background: transparent;
  font-weight: bold;
  width: 50px;
  color:${p => p.theme.colors.white};
  font-size:50px;
  cursor: pointer;
`

export default Modal;
