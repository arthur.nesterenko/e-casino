export type GameTypes = 'DICE' | 'GOAL' | 'MINES' | 'CLAMS ';

export interface IBet {
    id: string
    time: Date
    name: string
    game: GameTypes
    bet: number
    payout: number
    profit: number
}

export interface IBedTable {
    items: IBet[]
}
