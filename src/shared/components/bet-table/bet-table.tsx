import React, {FunctionComponent} from 'react';
import Head from './components/head'
import Row from './components/row'
import {Table} from './bed-table.styled'
import {IBedTable} from './types'


const BetTable: FunctionComponent<IBedTable> = ({items}) => {

    return <Table>
        <Head/>
        {items.map(item => <Row key={item.id} {...item} out={false}/>)}
    </Table>
};


export default BetTable;
