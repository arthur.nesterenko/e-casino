import styled from 'styled-components'

export const Thead = styled.div`
background-color: ${p => p.theme.colors.blue};
border: 0;
span{
color:#7D9CC6;
font-size: 15px;
line-height: 18px;
text-transform: uppercase;
font-weight: normal;
}

`
export const TRow = styled.div`
height: 50px;
display: grid !important;
padding: 0 20px 0 40px;
grid-template-columns: 180px 146px 123px 1fr;
@media (max-width: 768px){
grid-template-columns: 1fr 1fr;
grid-gap: 25px;
}
align-items: center;

small{
color:${p => p.theme.colors.white}
}
`;


export const Table = styled.div`
border-radius: 10px;
max-width: 602px;
width: 100%;
margin: auto;
overflow: hidden;
background-color: ${p => p.theme.colors.brand};

`


export const Cell = styled.div<{ hide?: boolean }>`
display: flex;
align-items: center;

 @media (max-width: 768px){
    display: ${p => p.hide ? 'none' : 'flex'}
  }
  
`;
