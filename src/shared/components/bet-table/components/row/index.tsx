import React, {FunctionComponent} from 'react'
import {IBet} from '../../types'
import {TRow, Cell} from '../../bed-table.styled'
import BitcoinIcon from "shared/components/icons/bitcoin-icon";
import Profit from "../profit";
import Fade from "shared/components/fade";


interface IRow extends IBet {
    out: boolean

}

const Row: FunctionComponent<IRow> = ({bet, payout, profit, time, out}) => {

    const date = new Date(time);

    return <Fade as={TRow} out={out}>
        <Cell>
            <small>{date.toLocaleDateString()} {date.toLocaleTimeString()}</small>
        </Cell>
        <Cell hide>
            <BitcoinIcon/>
            <small>{bet}</small>
        </Cell>
        <Cell hide>
            <small>x{payout.toLocaleString()}</small>
        </Cell>
        <Cell>
            <BitcoinIcon/>
            <Profit value={profit}>
                {profit}
            </Profit>
        </Cell>
    </Fade>

}


export default Row
