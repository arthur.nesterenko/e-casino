import React, {FunctionComponent} from 'react'
import styled from "styled-components";


const isNegative = (x: number): boolean => Math.sign(x) === -1;


const StyledProfit = styled.span<{ isNegative: boolean }>`
color:${p => p.theme.colors[p.isNegative ? 'critical' : 'green']};
font-size: 13px;
font-weight: bold;
`


interface IProfit {
    value: number
}

const Profit: FunctionComponent<IProfit> = ({value}) => {

    const isTrue = isNegative(value)

    return <StyledProfit isNegative={isTrue}>
        {!isTrue && "+"}{value}
    </StyledProfit>
}

export default Profit
