import React, {FunctionComponent} from 'react'
import {Thead, TRow, Cell} from '../../bed-table.styled'

const Head: FunctionComponent = () => {


    return <Thead>
        <TRow>
            <Cell>
                <span>Time</span>
            </Cell>
            <Cell hide>
                <span>Bet</span>
            </Cell>
            <Cell hide>
                <span>Multiplayer</span>
            </Cell>
            <Cell>
                <span>Profit</span>
            </Cell>
        </TRow>
    </Thead>
}

export default Head;
