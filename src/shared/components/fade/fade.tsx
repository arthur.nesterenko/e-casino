import React, {FunctionComponent} from 'react'
import StyledFade from './fade.styled'


const Fade: FunctionComponent<any> = ({children, ...props}) => {

    const [visible, setVisible] = React.useState(true)

    const onTransitionEnd = (e: any) => {
        setTimeout(() => setVisible(false), 200);
        console.log('onTransitionEnd', e)
    }

    return visible ? <StyledFade {...props} onTransitionEnd={onTransitionEnd}>
        {children}
    </StyledFade> : null


}


export default Fade;
