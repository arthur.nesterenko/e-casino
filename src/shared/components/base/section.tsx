import styled from "styled-components";
import media from "styled-media-query";

const Section = styled.section`


background-color: ${p => p.theme.colors.bg};
border-radius: 15px;
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
min-height: 50vh;
padding:  10px;

${media.greaterThan<any>("large")`
padding: 25px;
    
`}
`


export default Section;
