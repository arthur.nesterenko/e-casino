import styled from "styled-components";
import media from "styled-media-query";

const Main = styled.main`
background-color: ${p => p.theme.colors.dark};
height: 100%;
flex: 1 0 auto;
display: grid;
grid-template-columns:  1fr;
grid-gap: 50px;
row-gap: 30px;
padding: 14px;


${media.greaterThan<any>("large")`
    grid-template-columns:  max-content 1fr max-content;
padding:  92px;    
`}


`


export default Main;
