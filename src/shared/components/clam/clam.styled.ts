import styled, {keyframes} from "styled-components";
import {ReactComponent as ClamSvg} from './assets/clam.svg'


const openClam = keyframes`
from {
  transform: translate(-1143.213px, -333.598px);
  }

  to {
     transform: translate(-1143.213px, -373.598px);
  }
`


const StyledClam = styled(ClamSvg)`

cursor: pointer;
#clam_top{
transition: transform 1s linear;
animation-direction: reverse;
}

&:hover{
#clam_top{
animation: ${openClam} 1s linear;
transform: translate(-1143.213px, -373.598px);
}
}
`


export default StyledClam
