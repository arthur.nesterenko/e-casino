import React, {Fragment} from 'react'
import Modal from "../modal";
import StyledClam from './clam.styled'


const Clam = () => {

    const [isModalOpen, setModalOpen] = React.useState(false);


    return <Fragment>
        <StyledClam onClick={() => setModalOpen(true)}/>
        {isModalOpen &&
        <Modal isOpen={isModalOpen} title={<div>Confirm</div>} onClose={() => setModalOpen(false)}/>
        }
    </Fragment>
}


export default Clam
