import styled from "styled-components";


const Header = styled.header`
width: 100%;
height: 50px;
z-index: 20;
background-color: ${p => p.theme.colors.lightBlue};
  position: -webkit-sticky;
  position: sticky;
  top: -1px;
`


export default Header
