import React from 'react';
import {render} from './utils/test-utils';
import {getByTestId} from '@testing-library/dom'
import App from './App';
import {MockedProvider} from '@apollo/react-testing'
import {BET_SUBSCRIPTION} from './shared/hooks/use-bet-subscription'

const mocks = [
    {
        request: {
            query: BET_SUBSCRIPTION,

        },
        result: {
            data: {
                betAdded: {
                    time: new Date('2017 01 01'),
                    id: '1111'
                }
            }
        }
    },

]


test('renders App', () => {
    const {container} = render(<MockedProvider mocks={mocks}>
        <App/>
    </MockedProvider>);
    expect(getByTestId(container, 'main')).toBeInTheDocument();
});
